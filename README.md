# Aprendendo Django

Este é um repositório para acompanhar os exercícios do livro "Aprenda Django 3
com exemplos"

## Início do projeto:
* Criar uma aplicação:
    ```sh
    python manage.py startapp <nome_da_app>
    ```
* Incluir a aplicação na lista INSTALLED_APPS no arquivo settings.py
* No arquivo urls.py, mapear as views para os padrões de URL
* setar configurações de bancos de dados, linguagem e timezone:
    ```python
    LANGUAGE_CODE = 'pt-br'
    TIME_ZONE = 'America/Sao_Paulo'
    ```
   
:point_right: O serviço pode ser iniciado em servidor ou porta
diferente, sendo possível passar o arquivo settings como parâmetro:
**É possível utilizar diferentes arquivos de configuração de acordo com o ambiente - importante para CI/CD**
```sh
python manage.py runserver 127.0.0.1:8001 --settings=mysite.settings
```

## Configurações do projeto:

Todos os parâmetros para o `settings.py` podem ser encontrados
[aqui](https://docs.djangoproject.com/en/3.0/ref/settings/)

* DEBUG:
    * Exibe páginas de erro detalhadas e dados sigilosos;
    * Antes de migrar para produção, mudar o DEBUG;
    * pode buscar de uma variável de ambiente (CI/CD)

* ALLOWED_HOSTS:
    * Se o DEBUG estiver ativado, a configuração é ignorada;
    * Quando em produção, deve ser configurado o domínio.

**Aplicações X projetos**
* Um projeto é uma instalação django com algumas configurações;
* as aplicações são conjuntos de views, urls e templates, e são
incorporadas a um projeto, podendo ser reutilizadas em diversos
projetos.

