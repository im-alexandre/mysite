## Capítulo 2: recursos avançados

### Criação de formulários
Formulários podem ser criados a partir de duas classes base:
* `Form`: criar formulários padrão;
* `ModelForm`: criar formulários baseados em modelos.

:point_right: Por padrão, os formulários serão criados em um arquivo `forms.py`,
no diretório da aplicação.  
:point_right: o módulo `form` possui diversos tipos de campos `CharField, EmailField`, etc,
que podem ser utilizados nos formulários criados  
:point_right: Os formulários podem ser renderizados nas views de diversas formas.
podem ser passados de uma vez. Por exemplo, `{{ form.as_p }}` renderiza os campos do
formulário em quebras de linha HTML.  
:point_right: É possível, ainda iterar pelos campos do formulário e renderizar cada
um, exemplo:  
```html
{% for field in form %}
<div>
{{ field.errors }}
{{ field.label_tag }} {{ field }}
</div>
{% endfor %}
```
:point_right: ***Não esquecer da tag `{% crsf_token %}`***  
:point_right: Para criar um formulário a partir de um modelo, basta definir o modelo
na classe Meta.


### Enviando e-mail no Django
O framework possui recursos para lidar com o protocolo SMTP. As informações do
serviço de e-mail são inseridas no arquivo `settings.py`:  

```py
# Informações para o serviço de e-mail:
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'meu-email@gmail.com'
EMAIL_HOST_PASSWORD = 'minha_senha'
EMAIL_USE_TLS = True
EMAIL_PORT = 587

# Para exibir os emails no shell (serviço de desenvolvimento)
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
```
**Testando as configurações de e-mail**:
```sh
python manage.py shell
Python 3.8.5 (default, Sep  4 2020, 07:30:14) 
[GCC 7.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> from django.core.mail import send_mail
>>> send_mail('Django mail', 'Esse é um email do django', 'im.alexandre07@gmail.com', ['im.alexandre07@gmail.com'], fail_silently=False)
Content-Type: text/plain; charset="utf-8"
MIME-Version: 1.0
Content-Transfer-Encoding: 8bit
Subject: Django mail
From: im.alexandre07@gmail.com
To: im.alexandre07@gmail.com
Date: Wed, 24 Feb 2021 19:06:18 -0000
Message-ID: <161419357891.11912.9262359684153579587@CCIMAR-16>

Esse é um email do django
-------------------------------------------------------------------------------
1
```

### Buscando posts por similaridade:
Utilizando o `manage.py shell`:
```python
from django.db.models import Count
from blog.models import Post
post = Post.published.get(id=2)
ppost_tags_ids = post.tags.values_list('id', flat=True)
ost_tags_ids = post.tags.values_list('id', flat=True)
similar_posts = Post.published.filter(tags__in=post_tags_ids).exclude(id=post.id)
```


