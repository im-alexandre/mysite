## Criação de tags de template personalizadas

Existem 2 tipos de tags personalizadas:
* simple_tag: processa  os dados e devolve uma string
* inclusion_tag: processa dados e devolve um template renderizado
    * As tags de inclusão renderizam um template e retornam variáveis de contexto
    * Para utilizar uma tag de inclusão, deve ser criado um template específico,
que será passado como parâmetro para o decorador e renderizado pela tag.
    * Tags de inclusão devem retornar um dicionário, que será passado como 
contexto para o template a ser renderizado.

***Vantagem***: As tags personalizadas podem ser utilizadas a qualquer template,
independentemente da view que esteja sendo executada. (Por exemplo, uma tag pode
fazer uma query utilizando informações não disponíveis naquela view)

***Criação***  
```python
from django import template

from ..models import Post

register = template.Library()


@register.simple_tag
def total_posts():
    """Tag simples para retornar todos os posts publicados"""
    return Post.published.count()
```
Utilização nos templates:
```html
    <h2>My blog</h2>
    <p>This is my blog. I've written {% total_posts %} posts so far.</p>
```

## Filtros de template
Filtros servem para alterar variáveis nos templates. São funções python que podem
ser aplicadas nas variáveis dentro das tags dos templates, utilizando a seguinte
sintaxe:  
* Filtros sem argumento: `{{ variable|my_filter }}`
* Filtros com um argumento: `{{ variable|my_filter:"foo" }}`

### Criando um filtro para utilização de sintaxe markdown
Por padrão, o django não permite utilização de sintaxe HTML nos templates,
evitando que seja inserido código inseguro. Quando for necessário passar strings
html para um template, deve-se utilizar o scape ou o mark_safe()

## Adicionando mapa do site
Mapas de site são arquivos XML gerados dinamicamente. Eles ajudam os motores de
busca e, portanto melhoram a visibilidade dos sites nas ferramentas de busca (google).  
Para incluir o mapa de sites, deve-se ativar as aplicações sites e sitemaps nas
configurações do projeto. Exemplo:
```python
INSTALLED_APPS += [
    'blog.apps.BlogConfig',
    'taggit',
    'django.contrib.sites',
    'django.contrib.sitemaps',
]
```
[Neste link](https://www.codeproject.com/Questions/5257129/Why-does-my-sitemap-xml-page-return-django-issue-r), 
sugere que comentar a linha django.contrib.sites (não incluir esse app), caso contrário,
será retornado um erro quando acessar o mapa do site. Neste caso, não será possível
manipular o modelo `Sites` no admin.  
Outra solução é adicionar `SITE_ID = 1` antes de definir INSTALLED_APPS assim,
podemos adicionar o `django.contrib.sites`, manipular o `Sites` através do admin
e o sitemap.xml volta a funcionar.  


Para mapear um site, crie um módulo chamado sitemaps.py no diretório da aplicação,
conforme abaixo:
```python
from django.contrib.sitemaps import Sitemap

from .models import Post


class PostSitemap(Sitemap):
    """Classe para geração de mapas do site"""
    changefreq = 'weekly'
    priority = 0.9

    def items(self):
        """Devolve o queryset dos objetos a serem incorporados no mapa
        django itera pelos objetos e aplica a função get_absolute_url() em cada
        objeto retornado por esta função para retornar a URL de cada objeto"""
        return Post.published.all()

    def lastmod(self, obj):
        """Recebe cada objeto retornado por items() e retorna a data da última
        modificação"""
        return obj.updated
```

***Os atributos `changefreq` e `priority` podem ser métodos ou atributos.***
***Django já possui uma view para sitemap. Deve-se criar um padrão de URL no projeto***


## Feeds e RSS
Django tem um framework distribuidor de feeds (syndication), possibilitando gerar
feeds RSS automaticamente. Um feed é um formato de dados (normalmente XML), que
fornece os conteúdos mais recentemente alterados. Os usuários podem usar um agragador
de feeds para se inscrever no seu feed, recebendo notificações do seu blog, por
exemplo.  
Para criar o feed siga os passos:
* Criar uma classe baseada na classe Feed (ver blog/feeds.py)
* importar a classe criada e criar um padrão de url na aplicação (blog/urls.py)

## Pesquisa Full text e outros recursos do PostgreSQL
O módulo `django.contrib.postgres` possui funcionalidades específicas do postgres,
não suportadas por outros bancos aceitos pelo Django. Um exemplo é a pesquisa full
text.  
Neste ponto, mudaremos o banco de dados da nossa aplicação para o postgresql.   
Utilizando o docker-compose:  
```sh
docker-compose up --build -d
docker exec -it <nome_container> /bin/bash
su - postgres
psql
create extension pg_trgm
```
***Saia do container, migre novamente e crie o superuser novamente***:
```sh
python manage.py migrate
python manage.py createsuperuser
```

Mudando as configurações do django para usar o postgresql:
```python
INSTALLED_APPS += [
    'blog.apps.BlogConfig',
    'taggit',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django.contrib.postgres',
]
# ...
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'blog',
        'HOST': 'localhost',
        'PORT': 15432
    }
}
```

Com Django, é possível criar vetores para busca (buscar valores em mais de um campo
por vez). Usando o `manage.py shell`:
```python
from django.contrib.postgres.search import SearchVector
from blog.models import Post
Post.objects.annotate(search=SearchVector('title', 'body'),
).filter(search='django')
```

A busca full text é custosa. Para melhorar a performance, pode ser necessário criar
um índice funcional. O Django disponibiliza o SearchVectorField para os modelos.

### Stemming e classificação de resultados
Stemming significa reduzir as palavras a seus radicais. Numa busca, por exemplo,
músico e música retornaram como semelhantes. Django disponibiliza uma classe SearchQuery,
que faz buscas utilizando stemização) e outra chamada SearchRank, que ordena os 
resultados com base na quantidade de vezes que os termos aparecem e sua proximidade. Exemplo:
```python
from django.contrib.postgres.search import (SearchQuery, SearchRank,
                                            SearchVector)
search_vector = SearchVector('title', 'body')
search_query = SearchQuery(query)
results = Post.published.annotate(
search=search_vector,
rank=SearchRank(search_vector, search_query),
        ).filter(search=search_query).order_by('-rank')
```
Também é possível atribuir pesos às consultas, favorecendo resultados em um ou 
outro campo:
```python
search_vector = SearchVector('title', weight='A') + \
SearchVector('body', weight='B')
search_query = SearchQuery(query)
results = Post.published.annotate(
search=search_vector,
rank=SearchRank(search_vector, search_query),
        ).filter(rank__gte=0.3).order_by('-rank')
```
**Valores dos pesos**
* 'A': 1.0
* 'B': 0.4
* 'C': 0.2
* 'D': 0.1

## Pesquisa de semelhança por trigramas:
A similaridade é medida pelo número de trigramas que elas compartilham, ou seja,
a quantidade de conjuntos de 3 letras que podem ser formados em comum.  
É outra funcionalidade de busca suportada pelo postgresql dentro do django.  
Para instalar a extensão de suporte a trigramas no postgresql, execute o seguinte
comando no shel do psql:
```sql
CREATE EXTENSION pg_trgm;
```

**Testando:**
```python
from django.contrib.postgres.search import TrigramSimilarity
query = 'yango'
results = Post.published.annotate(similarity=TrigramSimilarity('title', query),
).filter(similarity__gt=0.1).order_by('-similarity')
```

### Outras ferramentas de pesquisa fulltext:
O Django disponibiliza o Haystack. Caso seja necessário utilizar outros motores
de busca, como o ElasticSearch e Solr, o Haystack é uma camada de abstração para
várias ferramentas do tipo, oferecendo uma API simples e parecida com os querysets.
