## Modelos:
* Utilizar o django.utils.timezone
* auto_now e auto_add_now. Exemplo:
    ```python
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    ```
* SlugField:
    * serve para criar urls mais elegantes;
    * pode ser concatenado com a data de criação do objeto. Exemplo:
    ```python
    slug = models.SlugField(max_length=250, unique_for_date='publish')
    ```
    * O código acima evita que sejam criadas slugs duplicados
* Usuários:
    * utilizar o módulo `django.contrib.auth.models.User` permite
    implementar autenticação e modelagem de usuários sem ter que
    "reinventar a roda".
* Meta:
    * é possível definir parâmetros para os modelos. No exemplo,
    os posts serão ordenados de maneira decrescente por data 
    de publicação:
    ```python
    class Meta:
        ordering = ('-publish', )

    def __str__(self):
        return self.title
    ```
    * a função `__str__` define qual a representação padrão dos
    objetos.
    * é possível definir o nome da tabela criada no banco de dados
    atribuindo o nome à variável db_table na classe Meta.
* Migração:
    * O comando `manage.py makemigrations` cria os scripts necessários
    para sincronizar as mudanças no banco de dados.
    * O comando `manage.py sqlmigrate` apenas imprime as instruções
    SQL geradas por uma migração específica. Exemplo:
        ```sh
        python manage.py sqlmigrate blog 0001
        ```

## URL Canônicos:
:point_right: URL canônico é a url que serve para encontrar um objeto. Para gerar
essa URL, é convencionado utilizar o método `get_absolute_url`, conforme o exemplo:
```python
def get_absolute_url(self):
    return reverse('blog:post_detail',
                   args=[
                       self.publish.year,
                       self.publish.month,
                       self.publish.day,
                       self.slug,
                   ])
```
:point_right: Para testar o método acima, utilize o `manage.py shell`:
```python
from blog.models import Post
post = Post.objects.get(id=2)
print(post.get_absolute_url()) # '/blog/2021/2/23/mais-um-post-brabo/'
```
        
## Administração
:point_right: O Django fornece uma página de administração
para gerenciar o conteúdo das aplicações (no caso, criando Posts,
por exemplo)  

* Criar superusuário - digite o comando abaixo e forneça as informações:
        ```sh
    python manage.py createsuperuser
    ```
    
### Adicionar modelos no site de administração:
* alterar o arquivo `admin.py`:
    ```python
    from django.contrib import admin
    from .models import Post

    @admin.register(Post)
    class PostAdmin(admin.ModelAdmin):
        list_display = ('title', 'slug', 'author', 'publish', 'status')
        list_filter = ('status', 'created', 'publish', 'author')
        search_fields = ('title', 'body')
        prepopulated_fields = {'slug': ('title',)}
        raw_id_fields = ('author',)
        date_hierarchy = 'publish'
        ordering = ('status', 'publish')
    ```
* Além de registrar a classe Post no admin, é possível personalizar
    a forma como as informações são apresentadas na página de gerenciamento
    para tal, basta usar o decorador e criar uma nova classe, definindo
    determinadas variáveis, conforme visto acima.

## Trabalhando com Querysets
Criando um objeto pelo shell:
```sh
python manage.py shell
```
```python
from django.contrib.auth.models import User
from blog.models import Post
user = User.objects.get(username='alexandre')
print(user) #<User: alexandre>
post = Post(title='Mais um Post muito Brabo',
             slug='mais-um-post-brabo',
             body='Esse aqui é brabo mesmo',
             author=user)

post.save()
```
Mudando os atributos de um objeto recuperado do banco:
```python
post.title = 'Mudando o título do post'
post.save()
```
**Um queryset pode ser filtrado por atributos da classe usando dunder (dois underscores):**
```python
outro_post = Post.objects.filter(publish__year=2021).exclude(title__startswith='Mais')
outro_post #<QuerySet [<Post: Mudando o título do post>]>
```
## Gerenciadores de Modelo
O gerenciador padrão é o objects, mas podem ser criados outros.
No exemplo, um manager para retornar todos os posts onde o status seja "published"
A sintaxe para utilização do manager é parecida com o objects: `Post.published.all()`
```python
from django.db import models
class PublishedManager(models.Manager):
    def get_queryset(self):
        return super(PublishedManager, self).get_queryset().filter(status='published')
```

## Views
As views recebem uma requisição como parâmetro, executam determinada lógica e
retornam dados, que podem ser renderizados em um tamplate.
[Exemplo](./blog/views.py)
Uma view é uma função do módulo `views.py`, após a criação da função, deve ser
registrada a respectiva url no arquivo `urls.py` da aplicação.  
**A função get_object_or_404 retorna um objeto ou uma exceção HTTP 404**

A melhor forma de definir as urls é utilizando módulos `urls.py` para cada aplicação,
isso facilita a sua reutilização. Após a criação das url de cada aplicação, elas
devem ser incluidas ao projeto da seguinte forma:
```python
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('blog/', include('blog.urls', namespace='blog')),
]
```
:point_right: O namespace deve ser único dentro do projeto. O código acima inclui
os padrões de URL presentes na aplicação 'blog'. Para referenciar as urls dentro
de namespaces, é utilizada a notação 'blog:post_list'  

## Templates
:point_right: O Django possui uma linguagem eficiente para incluir o contexto passado
pelas views nos templates e renderizá-los para apresentar esses dados. Para isso,
a função deve retornar a função render, passando como parâmetros, a requisição,
o nome do template e um dicionário de contexto.  
```python
def post_detail(request, year, month, day, post):
    post = get_object_or_404(Post, slug=post,
                                   status='published',
                                   publish__year=year,
                                   publish__month=month,
                                   publish__day=day)
    return render(request,
                  'blog/post/detail.html',
                  {'post': post})
```

```html
{% extends "blog/base.html" %}

{% block title %}{{ post.title }}{% endblock %}

{% block content %}
  <h1>{{ post.title }}</h1>
  <p class="date">
    Published {{ post.publish }} by {{ post.author }}
  </p>
  <!-- Utilizando um filtro no corpo do post -->
  {{ post.body|linebreaks }}
{% endblock %}
```
:point_right: Para aplicar um filtro a determinada variável, basta passar com a
notação acima, utilizando pipes ("|"). Podem ser aplicados diversos filtros e 
cada um será aplicado à saída do anterior.  

## Paginação
Conforme a aplicação cresce, pode se tornar inconveniente mostrar todos os objetos
na mesma página. Para isso, o Django possui recursos próprios de pagianção.  
Para implantar a paginação, a view deve ser alterada:
```python
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
def post_list(request):
    """view para listar os posts"""
    object_list = Post.published.all()
    paginator = Paginator(object_list, 3)  # Exibir 3 postagens por página
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    return render(request, 'blog/post/list.html', {
        'page': page,
        'posts': posts
    })
```
Neste projeto, foi criado um template `templates/pagination.html` que foi incluido
nos templates onde a paginação é necessária.
```html
<div class="pagination">
    <span class="step-links">
    {% if page.has_previous  %}
    <a href="?page{{ page.previous_page_number }}">Previous</a>
    {% endif %}
    <span class="current">
    Page {{ page.number }} of {{ page.paginator.num_pages }}.
    </span>
    {% if page.has_next %}
    <a href="?page={{ page.next_page_number }}">Next</a>
    {% endif %}
    </span>
</div>
```


## Views baseadas em classes (Class Based Views)
As views podem ser criadas por meio de classes. Django possui classes base que
permitem essa abordagem. Vantagens:  
:check: Evitar ifs para lidar com cada tipo de requisição  
:check: Permite heranças múltiplas, criando views reutilizáveis (mixins)   
Um exemplo é a ListView:
```python
from django.views.generic import ListView
class PostListView(ListView):
    """View baseada em classe para listar os posts"""
    queryset = Post.published.all()
    context_object_name = 'posts'
    paginate_by = 3
    template_name = 'blog/post/list.html'
```
**Definição das variáveis:**
* `queryset`: Define como serão recuperados os objetos do banco. Pode ser definido um modelo
`model = Post` e Django utilizaria Post.objects.all()
* `context_object_name`: Por padrão, o Django utiliza object_list para armazenar
o queryset. Definindo essa variável como `posts`, não será necessário alterar os
templates.
* `paginate_by`: Quantas instâncias serão exibidas por página.
* `template_name`: Se não for definido, Django utilizará `blog/post_list.html`  
:point_right: por padrão, o django armazena o número da página na variável page_obj,
que será utilizada nos templates.
