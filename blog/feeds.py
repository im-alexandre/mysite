from django.contrib.syndication.views import Feed
from django.template.defaultfilters import truncatewords
from django.urls import reverse_lazy

from .models import Post


class LatestPostFeed(Feed):
    title = 'My blog'
    link = reverse_lazy('blog:post_list')
    description = 'New posts of my blog'

    def items(self):
        """retorna os 5 últimos posts publicados"""
        return Post.published.all()[:5]

    def item_title(self, item):
        """
        recebe a saída de items() e retorna o título de cada objeto
        (no caso, post)
        """
        return item.title

    def item_description(self, item):
        """
        Recebe cada objeto (post) do método items() e retorna o corpo truncado
        """
        return truncatewords(item.body, 30)
