from django.contrib import admin

from .models import Post, Comment


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    # Campos mostrados no grid de posts
    list_display = ('title', 'slug', 'author', 'publish', 'status')
    # Campos disponíveis para filtros e busca
    list_filter = ('status', 'created', 'publish', 'author')
    search_fields = ('title', 'body')
    # Conforme o título é preenchido, a slug também é criada
    prepopulated_fields = {'slug': ('title', )}
    # Buscar author em nova janela (tabela com usuários)
    raw_id_fields = ('author', )
    # Pesquisa por data (hoje, há 7 dias ...)
    date_hierarchy = 'publish'
    ordering = ('status', 'publish')

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'post', 'created', 'active')
    list_filter = ('active', 'created', 'active')
    search_fields = ('name', 'email', 'body')

