from django.contrib.sitemaps import Sitemap

from .models import Post


class PostSitemap(Sitemap):
    """Classe para geração de mapas do site"""
    changefreq = 'weekly'
    priority = 0.9

    def items(self):
        """Devolve o queryset dos objetos a serem incorporados no mapa
        django itera pelos objetos e aplica a função get_absolute_url() em cada
        objeto retornado por esta função para retornar a URL de cada objeto"""
        return Post.published.all()

    def lastmod(self, obj):
        """Recebe cada objeto retornado por items() e retorna a data da última
        modificação"""
        return obj.updated
